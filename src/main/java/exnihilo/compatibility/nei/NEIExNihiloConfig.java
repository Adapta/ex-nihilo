package exnihilo.compatibility.nei;

import codechicken.nei.api.API;
import codechicken.nei.api.IConfigureNEI;
import exnihilo.data.ModData;

public class NEIExNihiloConfig implements IConfigureNEI {

	@Override
	public String getName() 
	{
		return ModData.NAME;
	}

	@Override
	public String getVersion() 
	{
		return ModData.VERSION;
	}

	@Override
	public void loadConfig() 
	{
		RecipeHandlerHammer handlerHammer = new RecipeHandlerHammer();
		RecipeHandlerSieve handlerSieve = new RecipeHandlerSieve();
		API.registerRecipeHandler(handlerHammer);
		API.registerUsageHandler(handlerHammer);
		API.registerRecipeHandler(handlerSieve);
		API.registerUsageHandler(handlerSieve);
		
	}

}
