package exnihilo.registries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import cpw.mods.fml.common.registry.GameRegistry;

import exnihilo.utils.ItemInfo;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.oredict.ShapedOreRecipe;
import exnihilo.ENBlocks;
import exnihilo.ENItems;
import exnihilo.registries.helpers.Smashable;

public class HammerRegistry {

	private static HashMap<ItemInfo, ArrayList<Smashable>> rewards = new HashMap<ItemInfo, ArrayList<Smashable>>();
	
	public static boolean allowIceShards = true;
	public static String categorySmashableOptions = "smashables";

	public static void register(Block source, int sourceMeta, Item output, int outputMeta, float chance, float luckMultiplier)
	{

		Smashable entry = new Smashable(source, sourceMeta, output, outputMeta, chance, luckMultiplier);
		ItemInfo ii = new ItemInfo(source, sourceMeta);
		ArrayList<Smashable> current;
		if (rewards.containsKey(ii))
			current = rewards.get(ii);
		else
			current = new ArrayList<Smashable>();
		
		current.add(entry);
		rewards.put(ii, current);
		//registryCache.put(new ItemInfo(new ItemStack(source, 1, sourceMeta)), true);
	}

	public static ArrayList<Smashable> getRewards(Block block, int meta)
	{
		return rewards.get(new ItemInfo(block, meta));
	}


	public static void load(Configuration config)
	{
		allowIceShards = config.get(categorySmashableOptions, "allowIceShards", true).getBoolean();
		if (allowIceShards)
		{
			register(Blocks.ice, 0, ENItems.IceShard, 0, 1.0f, 0.0f);
			register(Blocks.ice, 0, ENItems.IceShard, 0, 0.75f, 0.1f);
			register(Blocks.ice, 0, ENItems.IceShard, 0, 0.75f, 0.1f);
			register(Blocks.ice, 0, ENItems.IceShard, 0, 0.50f, 0.1f);
			register(Blocks.ice, 0, ENItems.IceShard, 0, 0.25f, 0.1f);
			register(Blocks.ice, 0, ENItems.IceShard, 0, 0.05f, 0.1f);
			
			GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(Blocks.packed_ice), "a a"," b ", "a a", 'a', new ItemStack(ENItems.IceShard), 'b', new ItemStack(Blocks.sand)));
			GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(Blocks.ice), "aa","aa", 'a', new ItemStack(ENItems.IceShard)));
		}
	}



	public static boolean registeredInefficiently(Block block, int meta)
	{
		Iterator<Smashable> it = rewards.get(new ItemInfo(block, meta)).iterator();
		while(it.hasNext())
		{
			Smashable reward = it.next();

			if (reward.source.getUnlocalizedName().equals(block.getUnlocalizedName()) && reward.meta == meta)
			{
				return true;
			}
		}

		return false;
	}



	public static boolean registered(ItemStack item)
	{
		return rewards.containsKey(new ItemInfo(item));
	}

	public static boolean registered(Block block, int meta)
	{
		return registered(new ItemStack(block, 1, meta));
	}

	public static void registerSmashables()
	{		
		register(Blocks.stone, 0, ENItems.Stones, 0, 1.00f, 0.0f);
		register(Blocks.stone, 0, ENItems.Stones, 0, 0.75f, 0.1f);
		register(Blocks.stone, 0, ENItems.Stones, 0, 0.75f, 0.1f);
		register(Blocks.stone, 0, ENItems.Stones, 0, 0.50f, 0.1f);
		register(Blocks.stone, 0, ENItems.Stones, 0, 0.25f, 0.1f);
		register(Blocks.stone, 0, ENItems.Stones, 0, 0.05f, 0.1f);

		register(Blocks.cobblestone, 0, Item.getItemFromBlock(Blocks.gravel), 0, 1.0f, 0.0f);
		register(Blocks.gravel, 0, Item.getItemFromBlock(Blocks.sand), 0, 1.0f, 0.0f);
		register(Blocks.sand, 0, Item.getItemFromBlock(ENBlocks.Dust), 0, 1.0f, 0.0f);

		register(Blocks.sandstone, 0, Item.getItemFromBlock(Blocks.sand), 0, 1.0f, 0.0f);
		register(Blocks.sandstone, 1, Item.getItemFromBlock(Blocks.sand), 0, 1.0f, 0.0f);
		register(Blocks.sandstone, 2, Item.getItemFromBlock(Blocks.sand), 0, 1.0f, 0.0f);

		register(Blocks.stonebrick, 0, Item.getItemFromBlock(Blocks.stonebrick), 2, 1.0f, 0.0f);

		register(Blocks.stonebrick, 2, ENItems.Stones, 0, 1.00f, 0.0f);
		register(Blocks.stonebrick, 2, ENItems.Stones, 0, 0.75f, 0.1f);
		register(Blocks.stonebrick, 2, ENItems.Stones, 0, 0.75f, 0.1f);
		register(Blocks.stonebrick, 2, ENItems.Stones, 0, 0.50f, 0.1f);
		register(Blocks.stonebrick, 2, ENItems.Stones, 0, 0.25f, 0.1f);
		register(Blocks.stonebrick, 2, ENItems.Stones, 0, 0.05f, 0.1f);

		register(Blocks.end_stone, 0, Item.getItemFromBlock(ENBlocks.EnderGravel), 0, 1.0f, 0.0f);
		register(Blocks.netherrack, 0, Item.getItemFromBlock(ENBlocks.NetherGravel), 0, 1.0f, 0.0f);
	}


	public static void registerOre(Block ore, int oreMeta, Item reward, int rewardMeta)
	{
		if (ore != null && reward != null) 
		{
			register(ore, oreMeta, reward, rewardMeta, 1.0f, 0.0f);
			register(ore, oreMeta, reward, rewardMeta, 1.0f, 0.0f);
			register(ore, oreMeta, reward, rewardMeta, 1.0f, 0.0f);
			register(ore, oreMeta, reward, rewardMeta, 1.0f, 0.0f);
			register(ore, oreMeta, reward, rewardMeta, 0.5f, 0.1f);
			register(ore, oreMeta, reward, rewardMeta, 0.05f, 0.1f);
			register(ore, oreMeta, reward, rewardMeta, 0.0f, 0.05f);
		}
	}
	
	public static ArrayList<ItemInfo> getSources(ItemStack reward)
	{
		ArrayList<ItemInfo> res = new ArrayList<ItemInfo>();
		
		for (ItemInfo entry : rewards.keySet())
		{
			for (Smashable smash : rewards.get(entry))
			{
				if (new ItemInfo(smash.item, smash.meta).equals(new ItemInfo(reward)))
					res.add(entry);
			}
		
			
		}
		return res;
	}
}
