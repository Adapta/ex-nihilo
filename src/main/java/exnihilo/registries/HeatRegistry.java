package exnihilo.registries;

import java.util.HashMap;
import java.util.Hashtable;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import exnihilo.registries.helpers.HeatSource;
import exnihilo.utils.ItemInfo;

public class HeatRegistry {
	@Deprecated
	public static Hashtable<String, HeatSource> entries = new Hashtable<String, HeatSource>();
	
	private static HashMap<ItemInfo, Float> heatmap = new HashMap<ItemInfo, Float>(); 
	
	/**
	 * Adds a new block to the heat registry for melting with crucibles
	 * @param block Block to be registered
	 * @param meta Metadata of block to be registered
	 * @param value Heating Rate
	 */
	public static void register(Block block, int meta, float value)
	{
		heatmap.put(new ItemInfo(block, meta), value);
	}
	
	/**
	 * Adds a new block and all meta's (0-16) for melting with crucibles.
	 * @param block Block to be registered
	 * @param value Heating Rate
	 */
	public static void register(Block block, float value)
	{
		for(int x = 0; x <= 15; x++)
		{
			register(block, x, value);
		}
	}
	
	/**
	 * Checks if block is registered heat source
	 * @param block Block to be checked
	 * @param meta Meta of block to be check
	 * @return true if block is a valid heat source.
	 */
	public static boolean containsItem(Block block, int meta)
	{
		return heatmap.containsKey(new ItemInfo(block, meta));
	}
	
	@Deprecated
	public static HeatSource getItem(Block block, int meta)
	{
		return entries.get(block + ":" + meta);
	}
	
	/**
	 * 
	 * @param block Block to get speed for
	 * @param meta Meta to get speed for
	 * @return Heating speed for block/meta combination. Returns 0 if the block/meta is not registered.
	 */
	public static float getSpeed(Block block, int meta)
	{
		if (heatmap.get(new ItemInfo(block, meta)) == null)
			return 0f;
		return heatmap.get(new ItemInfo(block, meta));
	}
	
	public static void registerVanillaHeatSources()
	{
		register(Blocks.torch, 0.1f);
		register(Blocks.lava, 0.2f);
		register(Blocks.flowing_lava, 0.1f);
		register(Blocks.lit_furnace, 0.15f);
		register(Blocks.fire, 0.3f);
	}
	
	/**
	 * Removes a block from the heat registry for heating crucibles etc
	 * @param block Block to be removed
	 * @param meta Metadata of block to be removed.
	 */
	public static void unregister(Block block, int meta)
	{
		entries.remove(block + ":" + meta);
		heatmap.remove(new ItemInfo(block, meta));
	}
	
	/**
	 * Removes a block and all metadatas from the heat registry for heating crucibles etc
	 * @param block Block to be removed
	 */
	public static void unregister(Block block)
	{
		for(int x = 0; x <= 15; x++)
		{
			unregister(block, x);
		} 
	}
	
	/**
	 * Replaces the melt speed of an already registered item
	 * @param block Block to be modified
	 * @param meta Meta of block to be modified
	 * @param newHeatValue New heat value to be inserted
	 */
	public static void modify(Block block, int meta, float newHeatValue)
	{
		if (entries.containsKey(block+":"+meta))
		{
			HeatSource src = entries.get(block+":"+meta);
			src.value = newHeatValue;
			entries.put(block+":"+meta, src);
		}
		
		ItemInfo info = new ItemInfo(block, meta);
		if (heatmap.containsKey(info))
		{
			heatmap.put(info, newHeatValue);
		}
	}
}
